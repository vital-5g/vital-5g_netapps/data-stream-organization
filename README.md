## Name
Data stream organization

## Description
VA6 - Data stream organization aids in organizing the data streams that are coming from different sensors or from video streaming (as available from VS9 currently, but available from any other external NetApp or 3rd party) so that they are transmitted over a 5G network and permit the sensing system to be used by specific services and applications.

## Build

The project is fully dockerized and all the components are part of the `docker-compose.yml` in the root.

In order to build the project, one should execute:

```
docker-compose build
```

## Run

To run the stack in the background you can use:

```
docker-compose up -d
```

## Ports

All the services are exposed through default ports as following:

```
MQTT Broker - 1883
InfluxDB - 8086
```


## Support
For support, contact [proiecte@beia.ro](mailto:proiecte@beia.ro).

## Roadmap
If you have ideas for releases in the future, it is a good idea to list them in the README.


## Authors and acknowledgment
This is the work of BEIA CONSULT INTERNATIONAL SRL's  staff in the context of the VITAL-5G Project.

## License
GPLv3

