# Beia IoT Stack

## Description

TBA

## Build

The project is fully dockerized and all the components are part of the `docker-compose.yml` in the root.

In order to build the project, one should execute:

```
docker-compose build
```

## Run

To run the stack in the background you can use:

```
docker-compose up -d
```

## Ports

All the services are exposed through default ports as following:

```
MQTT Broker - 1883
InfluxDB - 8086
```

## Generating sample data

See [dummy-data-generator](../dummy-data-generator).